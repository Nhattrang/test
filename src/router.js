import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);


const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/auth",
      name: 'auth',
      component: () => import("./pages/auth/index"),
      redirect: '/auth/login',
      children:[
        {
          path: "/auth/login",
          name: 'auth.login',
          component: () => import("./pages/auth/login"),
        },
        {
          path: "/auth/register",
          name: 'auth.register',
          component: () => import("./pages/auth/register"),
        },
      ]
    },
    {
      path: "/",
      name: "admin",
      component: () => import("./pages"),
      redirect: '/admin/home',
      children: [
        {
          path: "/admin/home",
          name: 'admin.home',
          component: () => import("./pages/home"),
        },
        {
          path: "/admin/users",
          name: 'admin.users',
          component: () => import("./pages/users"),
          redirect: '/admin/users/list',
          children: [
            {
              path: "/admin/users/list",
              name: 'admin.users.list',
              component: () => import("./pages/users/list.vue"),
            },
            {
              path: "/admin/users/create",
              name: 'admin.users.create',
              component: () => import("./pages/users/create.vue"),
            },
            {
              path: "/admin/users/profile/:id",
              name: 'admin.users.profile',
              component: () => import("./pages/users/profile.vue"),
            }
          ]
        },
        {
          path: "/admin/config",
          name: 'admin.config',
          component: () => import("./pages/config/index"),
          redirect: '/admin/config/list',
          children:[
            {
              path: "/admin/config/list",
              name: 'admin.config.list',
              component: () => import("./pages/config/list"),
            },
            {
              path: "/admin/config/create",
              name: 'admin.config.create',
              component: () => import("./pages/config/create"),
            },
            {
              path: "/admin/config/update/:id",
              name: 'admin.config.update',
              component: () => import("./pages/config/update"),
            },
          ]
        },
        {
          path: "/admin/product",
          name: 'admin.product',
          component: () => import("./pages/product/index"),
          redirect: '/admin/product/list',
          children:[
            {
              path: "/admin/product/list",
              name: 'admin.product.list',
              component: () => import("./pages/product/list"),
            },
            {
              path: "/admin/product/create",
              name: 'admin.product.create',
              component: () => import("./pages/product/create"),
            },
            {
              path: "/admin/product/update",
              name: 'admin.product.update',
              component: () => import("./pages/product/update"),
            },
          ]
        },
        {
          path: "/admin/order",
          name: 'admin.order',
          component: () => import("./pages/order/index"),
          redirect: '/admin/order/list',
          children:[
            {
              path: "/admin/order/list",
              name: 'admin.order.list',
              component: () => import("./pages/order/list"),
            },
            {
              path: "/admin/order/create",
              name: 'admin.order.create',
              component: () => import("./pages/order/create"),
            },
            {
              path: "/admin/order/order-details",
              name: 'admin.order.order-details',
              component: () => import("./pages/order/order-details"),
            },
            {
              path: "/admin/order/update/:id",
              name: 'admin.order.update',
              component: () => import("./pages/order/update"),
            },
          ]
        },
        {
          path: "*",
          redirect: '/admin/home'
        },
      ]
    },
  ]
});
router.beforeEach((to,from,next) => {
  if (!localStorage.getItem('token') && to.name!= 'auth.login') {
    next({name: 'auth.login'})
  }
  if(localStorage.getItem('token') && to.name == 'auth.login'){
    next({name:'admin'})
  }
  else next()
})
export default router