import axios from 'axios';
const login = {
    namespaced: true,
    actions: {
        async LoginUsers(_, body) {
            let response = await axios({
              method: 'post',
              url: 'http://localhost:3000/api/user/login',
              data: body,
            });
            return response
        },
        async Register(_, user) {
            let response = await axios({
              method: 'post',
              url: 'http://localhost:3000/api/user/create',
              data: user,
            });
            return response
        },
    }
}

export default login