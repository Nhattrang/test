import Vue from "vue";
import vuex from 'vuex';
import config from "./config";
import product from "./product"
import order from "./order";
import login from "./login";
import user from "./user";

Vue.use(vuex)
export default new vuex.Store({
        modules:{
            config,
            product,
            order,
            login,
            user
        }
})
