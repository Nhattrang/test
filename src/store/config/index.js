import axios from 'axios';

const config = {
  namespaced: true,
  actions: {
    async GetConfig(_, options) {
      let response = await axios({
        method: 'get',
        url: `http://localhost:3000/api/config/find`,
        params: {
          ...options,
          cauHinh: "danhmuc"
        }
      });
      return response.data
    },
    async ConfigCreate(_, config) {
      let response = await axios({
        method: 'post',
        url: 'http://localhost:3000/api/config/create',
        data: config,
      });
      return response
    },
    async UpdateConfig(_, config) {
      let response = await axios({
        method: 'post',
        url: `http://localhost:3000/api/config/update`,
        data: config
      });
      return response.data
    },
    async DeleteConfig(_, id) {
      let response = await axios({
        method: 'post',
        url: `http://localhost:3000/api/config/delete`,
        data: {
          id: id
        }
      });
      return response.data
    },
    async GetOneConfig(_, id) {
      let response = await axios({
        method: 'get',
        url: `http://localhost:3000/api/config?id=${id}`,
      });
      return response.data
    },
  }
}

export default config